package com.example.inventory.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping( "/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("[id]")
    public ResponseEntity<Product> get(@PathVariable Integer id) {
        Optional<Product> product = productService.get(id);
        if (product.isPresent()) {
            return ResponseEntity.ok(product.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Product> add(@RequestBody Product product) {
        Product addedProduct = productService.add(product);
        return ResponseEntity.ok(addedProduct);
    }
}

