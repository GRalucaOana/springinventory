package com.example.inventory.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepoitory productRepoitory;

    @Override
    public Product add(Product product) {
        return productRepoitory.save(product);
    }

    @Override
    public void delete(Integer id) {
        productRepoitory.deleteById(id);
    }

    @Override
    public Product update(Product product) {

        if (product.getId() == null) {
            throw new IllegalArgumentException("Product id is null");
        }
        return productRepoitory.save(product);
    }

    @Override
    public Optional<Product> get(Integer id) {
        return productRepoitory.findById(id);
    }

    @Override
    public List<Product> get() {
        return productRepoitory.findAll();
    }
}
