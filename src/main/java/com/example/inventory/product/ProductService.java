package com.example.inventory.product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Product add(Product product);

    void delete(Integer id);

    Product update(Product product);

    Optional<Product> get(Integer id);

    List<Product> get();


}
